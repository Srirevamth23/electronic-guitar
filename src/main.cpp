#include <Arduino.h>
#include <Bounce2.h>

// The lines for the Serial2 interface on an az-delivery-devkit-v4 ESP32
#define RXD2 16
#define TXD2 17

// The STRINGINDEX is the only variable that needs to be changed for the different microcontrollers.
// STRINGINDEX = 0 is the lowest string and also the microcontroller that is connected to the PC via serial
#define STRINGINDEX 0

// The mode bytevalues are choosen to be out of the range of midi that we use
#define GUITAR_MODE 252
#define SUSTAINED_GUITAR_MODE 253
#define KEYBOARD_MODE 254
#define SUSTAINED_KEYBOARD_MODE 255

// btn5 used to be 16
// btn6 used to be 17
// They got reassigned when the decision was made to use the Serial2 interface instead if I2C
int btn1 = 15, btn2 = 2, btn3 = 0, btn4 = 4, btn5 = 21, btn6 = 22, btn7 = 5, btn8 = 18, btn9 = 19, btn10 = 23, btn11 = 10, btn12 = 9, btn13 = 13, btn14 = 12, btn15 = 14, btn16 = 27, btn17 = 26, btn18 = 25, btn19 = 33, btn20 = 32, btn21 = 35, btn22 = 34, btn23 = 39;

// Make sure to only use a btn count of 20 to test with bare ESP 32's.
// The last 3 buttons have no internal pull up resistors and will therefore return wrong button presses
int buttonCount = 23;
int buttons[23] = {btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btn10, btn11, btn12, btn13, btn14, btn15, btn16, btn17, btn18, btn19, btn20, btn21, btn22, btn23};
Bounce bounceButtons[23];

int emptyNotes[6] = {40, 45, 50, 55, 59, 64};
int lastNotePlayed = 0;
int lastNoteButtonIndex = 0;
int timeoutBetweenCommands = 10;

int offVelocity = 127;

byte currentMode = GUITAR_MODE;

// All the controllers are daisy chained with their Serial2 interfaces.
// The first controller has nothing connected to its RX line, and its TX line connects to the second controller RX line
// The second controllers TX line connects to the third controllers RX line and so on.
// The 6th controller has nothing conneted to its TX line, but this controller is conencted to the PC via Serial
void sendMidiMessage(byte status, byte data1, byte data2)
{
  // For the controllers that are not connected to a pc, the message is send into the void
  // The controller that is connected to the pc sends it to the pc
  Serial.write(status);
  Serial.write(data1);
  Serial.write(data2);

  // For the controllers that are not connected to a pc, the message is passed down the daisy chain
  // The controller that is connected to the pc sends the message into the void
  Serial2.write(status);
  Serial2.write(data1);
  Serial2.write(data2);
}

bool isButtonPressed(uint8_t buttonState)
{
  // All the inputs have pull up resistors to 3.3V. 
  // If a button is pressed, the input is connected to GND. 
  // LOW => pressed
  if (buttonState == LOW) 
    return true;
  else
    return false;
}

void setup()
{
  Serial.begin(115200);

  pinMode(RXD2, INPUT);
  digitalWrite(RXD2, HIGH);

  Serial2.begin(115200, SERIAL_8N1, RXD2, TXD2);

  // Initialize all buttons
  for (int i = 0; i < buttonCount; i++)
  {
    // Define as input with internal pull up
    pinMode(buttons[i], INPUT);
    digitalWrite(buttons[i], HIGH);

    // Create bounce object for every button
    Bounce bounce = Bounce();
    bounce.attach(buttons[i], INPUT_PULLUP);
    bounce.interval(10);
    bounceButtons[i] = bounce;
  }

  // By pressing the trigger and mute buttons during startup, we can set the mode of the device
  // By default, the controller is programmed to behave like a guitar, so no need to have a case for this config
  bool isMuteButtonPressed = isButtonPressed(bounceButtons[0].read());
  bool isTriggerButtonPressed = isButtonPressed(bounceButtons[1].read());

  if (!isMuteButtonPressed && !isTriggerButtonPressed) currentMode = GUITAR_MODE;
  if (isMuteButtonPressed && !isTriggerButtonPressed) currentMode = SUSTAINED_GUITAR_MODE;
  if (!isMuteButtonPressed && isTriggerButtonPressed) currentMode = KEYBOARD_MODE;
  if (isMuteButtonPressed && isTriggerButtonPressed) currentMode = SUSTAINED_KEYBOARD_MODE;

  if (currentMode != GUITAR_MODE)
  {
    // Also forward configuration to next controller
    delay(100);
    Serial2.write(currentMode);
  }
}

int getNoteForButtonIndex(int buttonIndex)
{
  if (buttonIndex == 1)
    return emptyNotes[STRINGINDEX];
  else
    return emptyNotes[STRINGINDEX] + 23 - buttonIndex;
}

void muteLastNote()
{
  // Note 0 = no note played
  if (lastNotePlayed == 0)
    return;

  // Send the command to mute the note
  sendMidiMessage(128, lastNotePlayed, offVelocity);

  // Set last played note to 0 to avoid muting it again
  lastNotePlayed = 0;
}

void playNote(int note, int noteIndex)
{
  // Playing a note: Mute last note, wait a bit, play new note
  // The delay is extremly important for the DAW I tested with, because turning a note off and on at exactly the same time caused the audio to glitch
  muteLastNote();
  delay(timeoutBetweenCommands);
  sendMidiMessage(144, note, 127);
  lastNotePlayed = note;
  lastNoteButtonIndex = noteIndex;
}

void sustainedGuitarModeLoop()
{
  // Button 1 is the trigger button.
  // If we did not press the trigger button, there is nothing else to do in sustained guitar mode
  if (!bounceButtons[1].fell())
    	return;

  // When button 1 gets pressed in guitar or sustained guitar mode, we want to loop throught the note buttons and see if any note is pressed as well
  bool foundNote = false;

  // We skip buttons 0 and 1 since they are the trigger and mute buttons
  // Then we search from the highest note to the lowest note and play the highest pressed note
  for (int i = 2; i < buttonCount; i++)
  {
    if (!foundNote && bounceButtons[i].read() == LOW)
    {
      foundNote = true;

      playNote(getNoteForButtonIndex(i), i);

      // No need to waste cpu cycles when we found a note, so we just stop the loop
      break;
    }
  }

  // In case we checked all the notes and none was pressed, we want to play the site empty
  if (!foundNote)
    playNote(getNoteForButtonIndex(1), 1);
}

void guitarModeLoop()
{
  // Do everything the same way as the sustained guitar mode
  sustainedGuitarModeLoop();

  // But also, we also want to check if a previously pressed note got released
  // Note that the fist note (index 1) is the empty site. It must not get muted when button is released
  if (lastNoteButtonIndex != 1 && 
      lastNotePlayed == getNoteForButtonIndex(lastNoteButtonIndex) && 
      bounceButtons[lastNoteButtonIndex].rose())
  {
    // If the button of the played note on the site got released, we want to mute the note
    muteLastNote();
  }
}

void sustainedKeyboardModeLoop()
{
  // In sustained keyboard mode, we want to loop over all other notes and check if any note got pressed. If so, we play it. (Regardless of the trigger button)
  // Here, we skip button 0 since 0 is the mute button
  for (int i = 1; i < buttonCount; i++)
  {
    if (bounceButtons[i].fell())
    {
      playNote(getNoteForButtonIndex(i), i);
    }
  }
}

void keyboardModeLoop()
{
  sustainedKeyboardModeLoop();

  // In keyboard mode, we also want to check if a previously pressed note got released
  // If the button of the played note on the site got released, we want to mute the note
  if (lastNotePlayed == getNoteForButtonIndex(lastNoteButtonIndex) && 
      bounceButtons[lastNoteButtonIndex].rose())
  {
    muteLastNote();
  }
}

void loop()
{
  // Update all bounce objects
  for (int i = 0; i < buttonCount; i++)
    bounceButtons[i].update();

    // Button 0 is the Note off btn to mute the site, always
  if (bounceButtons[0].fell())
    muteLastNote();

  // Run the matching checks based on what the current mode is
  switch (currentMode)
  {
    case GUITAR_MODE:
      guitarModeLoop();
      break;

    case SUSTAINED_GUITAR_MODE:
      sustainedGuitarModeLoop();
      break;

    case KEYBOARD_MODE:
      keyboardModeLoop();
      break;

    case SUSTAINED_KEYBOARD_MODE:
      sustainedKeyboardModeLoop();
      break;
  }

  // Daisy chain magic.
  // Read everything that comes in on Serial2 and forward it on Serial2 but also on Serial
  if (Serial2.available())
  {
    byte recievedByte = Serial2.read();

    // We check for our special bytes, which we want to use, but not send to the pc
    if (recievedByte == GUITAR_MODE || 
        recievedByte == SUSTAINED_GUITAR_MODE || 
        recievedByte == KEYBOARD_MODE ||
        recievedByte == SUSTAINED_KEYBOARD_MODE)
      currentMode = recievedByte;
    else
      Serial.write(recievedByte);

    // We forward every byte to the next controller (also our special bytes)
    // Note that the serial 2 chain has an open end. This means the last controller sends it into the void
    // But usually, this is also the controller which is connected to the pc which interpretes the command as midi
    Serial2.write(recievedByte);
  }
}
